import * as THREE from 'three';


class ImagePlate {
  public static create(width: number, height: number, color: number): THREE.Mesh {
    let texture = App.getInstance().getTexture("assets/circle.png");
    let geometry = new THREE.PlaneGeometry(width, height);
    let material = new THREE.MeshBasicMaterial({
      color: color,
      transparent: true,
      map: texture
    });

    return new THREE.Mesh(geometry, material);
  }
}

class BillBoard {
  public static create(color: number): THREE.Sprite {
    let texture = App.getInstance().getTexture("assets/circle.png");
    let material = new THREE.SpriteMaterial({
      color: color,
      blending: THREE.AdditiveBlending,
      transparent: true,
      map: texture
    });

    return new THREE.Sprite(material);
  }
}





class App {
  private static _inst: App = new App();

  public static getInstance(): App {
    return this._inst;
  }

  public getTexture(path: string): THREE.Texture {
    const loader = new THREE.TextureLoader();
    return loader.load(path);
  }

  private _scene: THREE.Scene;
  private _plate: THREE.Mesh;
  private _billBoard: THREE.Sprite;

  private _camera: THREE.Camera;
  private _rot: number = 0;

  run() {
    const renderer = new THREE.WebGLRenderer();
    renderer.setSize(800, 600);
    document.body.appendChild(renderer.domElement);

    this._scene = new THREE.Scene();
    this._camera = new THREE.PerspectiveCamera(45, 800 / 600, 1, 10000);
    this._camera.position.set(0, 0, 1000);
    this._camera.position.y = 20;

    this._plate = ImagePlate.create(20, 20, 0xFF0000);
    this._plate.position.y = 30;
    this._scene.add(this._plate);


    this._billBoard = BillBoard.create(0xFF0000);
    this._billBoard.scale.multiplyScalar(10);
    this._scene.add(this._billBoard);

    const grid = new THREE.GridHelper(100, 10);
    this._scene.add(grid);

    const axis = new THREE.AxisHelper(100);
    this._scene.add(axis);



    // 平行光源を生成
    const light = new THREE.DirectionalLight(0xffffff);
    light.position.set(1, 1, 1);
    this._scene.add(light);

    const tick = (): void => {
      requestAnimationFrame(tick);
      this.update();
      renderer.render(this._scene, this._camera);
    };
    tick();
  }


  update() {
    this._plate.rotateZ(0.005);
    this._plate.scale.addScalar(0.001);

    this._rot += 0.005;
    this._camera.position.x = 80 * Math.cos(this._rot);
    this._camera.position.z = 80 * Math.sin(this._rot);
    this._camera.lookAt(new THREE.Vector3(0,0,0));
  }

}

const app = new App();
window.addEventListener('DOMContentLoaded', () => {
  app.run();
});
